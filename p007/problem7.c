#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>

#define NTH_PRIME 10001

int main()
{
    unsigned int *listOfPrimes = malloc(sizeof(unsigned int) * NTH_PRIME);
    memset(listOfPrimes, 0xFF, sizeof(unsigned int) * NTH_PRIME);

    listOfPrimes[0] = 2;

    unsigned int counter = 1;
    unsigned int tmp = 3;
    while (counter < NTH_PRIME) {
        unsigned int s = (unsigned int) ceil(sqrt(tmp))+1;
        // check primarity for tmp
        unsigned int primeIndex = 0;
        while (listOfPrimes[primeIndex] < s && 
               tmp % listOfPrimes[primeIndex] != 0)
        {
            primeIndex++;
        }
        if (listOfPrimes[primeIndex] >= s) {
            listOfPrimes[counter] = tmp;
            counter++;
        }
        tmp += 2;
    }

    printf("The %dth prime is %d\n", NTH_PRIME, listOfPrimes[NTH_PRIME-1]);
}
