#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>


void sum_arrays(uint16_t *op1, uint32_t *len_array)
{
    uint16_t tmp = op1[0];
    for (uint32_t i = 0; i < *len_array - 1; i++) {
        op1[i] = op1[i] + tmp;
        tmp = op1[i+1];
        if (op1[i] > 9) {
            op1[i+1]++; // prova +1 enlloc de op1[i]/10
            op1[i] -= 10; // prova de restar enlloc de %
        }
    }
}

void sum_arrays_10000(uint16_t *op1, uint32_t *len_array)
{
    uint16_t tmp = op1[0];
    for (uint32_t i = 0; i < *len_array - 1; i++) {
        op1[i] = op1[i] + tmp;
        tmp = op1[i+1];
        if (op1[i] > 9999) {
            op1[i+1]++;
            op1[i] -= 10000;
        }
    }
}

uint32_t final_sum(uint16_t *op1, int len_array)
{
    uint32_t sum = 0;
    for (int i = 0; i < len_array; i++) {
        sum += op1[i];
    }
    return sum;
}

uint32_t final_sum_10000(uint16_t *op1, int len_array)
{
    uint32_t sum = 0, tmp;
    for (int i = 0; i < len_array; i++) {
        sum += op1[i]%10; // unitats
        sum += (op1[i]/10)%10; // desenes
        sum += (op1[i]/100)%10; // centenes
        sum += op1[i]/1000; // milers
    }
    return sum;
}

void print_array(uint16_t *op1, int len_array)
{
    for (int i = 0; i < len_array; i++) {
        printf("%d ", op1[i]);
    }
    printf("\n");
}

int main()
{
    uint32_t base = 2;
    uint32_t power = 100000;

    // el +3 és de marge. /3 i n'afegim 3
    uint32_t max_digits = (uint32_t) ceil(power * log10(base)) / 4 + 4; // 10 ^4 = 10000

    uint16_t *operand1 = (uint16_t *) calloc(sizeof(uint16_t), max_digits);

    operand1[0] = 1;

    for (int i = 1; i <= power; i++) {
        // només sumarem els números != 0 dins l'array
        max_digits = (uint32_t) ceil(i * log10(base)) / 4 + 4;
        sum_arrays_10000(operand1, &max_digits);
        //print_array(operand1, max_digits);
    }

    printf("Final sum: %d\n", final_sum_10000(operand1, max_digits));
    free(operand1);

    return 0;

}
