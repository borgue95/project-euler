#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define MAX 2000000 // 2M

int *get_primes(int *size)
{
    int *initial_list = malloc(sizeof(int) * (MAX + 1));

    for (int i = 0; i <= MAX; i++) {
        initial_list[i] = i;
    }

    initial_list[0] = 0;
    initial_list[1] = 0;

    int sqt = ceil(sqrt(MAX));
    for (int i = 2; i <= sqt; i++) {
        if (initial_list[i] != 0) {
            for (int j = i * 2; j <= MAX; j += i) {
                initial_list[j] = 0;
            }
        }
    }

    int num_of_primes = 0;
    for (int i = 0; i <= MAX; i++) {
        num_of_primes += initial_list[i] != 0;
    }

    int *final_list = malloc(sizeof(int) * num_of_primes);
    int j = 0;
    for (int i = 0; i <= MAX; i++) {
        if (initial_list[i] != 0) {
            final_list[j] = initial_list[i];
            j++;
        }
    }

    free(initial_list);

    *size = num_of_primes;
    return final_list;
}

int main()
{
    int size;
    int *primes = get_primes(&size);

    unsigned long sum = 0;
    for (int i = 0; i < size; i++) {
        sum += (unsigned long) primes[i];
    }
    printf("Sum of all primes below 2.000.000: %lu\n", sum);

    return 0;
}
