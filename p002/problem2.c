#include <stdio.h>

int main()
{
    int a = 1;
    int b = 2;
    int c = 0;
    int accum = 2;

    int max = 4000000; // 4 milion

    while (c < max) {
        c = a + b;
        a = b;
        b = c;
        if ((c & 1) == 0) {
            accum += c;
        }
    }

    printf("Value: %d\n", accum);

    return 0;
}
