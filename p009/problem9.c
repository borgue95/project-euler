#include <stdio.h>

#define SUM 1000

int main()
{
    for (int c = SUM; c > 2; c--) {
        for (int b = c-1; b > 1; b--) {
            for (int a = b-1; a > 0; a--) {
                if (a+b+c==SUM) {
                    if (a*a + b*b == c*c) {
                        printf("Triplet: %d %d %d\n", a, b, c);
                        printf("Product (a*b*c): %ld\n", (long)a*b*c);
                    }
                }
            }
        }
    }
}
