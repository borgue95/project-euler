#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main()
{
    int max = 100;
    int64_t sum = 0;
    int64_t squared_sum = 0;
    int64_t sum_of_squares = 0;
    int64_t result;

    for (int i = 0; i <= max; i++) {
        sum += i;
        sum_of_squares += i*i;
    }
    squared_sum = sum * sum;
    result = squared_sum - sum_of_squares;
    printf("Result calculated: \t%ld\n", result);

    // mathematically:
    sum = (max*(max+1))/2;
    squared_sum = sum * sum;
    sum_of_squares = (max*(max+1)*(2*max+1))/6;
    result = squared_sum - sum_of_squares;
    printf("Result mathematically: \t%ld\n", squared_sum - sum_of_squares);
}
