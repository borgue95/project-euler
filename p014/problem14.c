#include <stdio.h>
#include <stdint.h>

#define MAX 1000000

uint64_t apply_collatz(int start)
{
    uint64_t count = 1;
    uint64_t tmp = start;
    while (tmp > 1) {
        if (tmp % 2 == 0) {
            tmp /= 2;
        } else {
            tmp = 3 * tmp + 1;
        }
        count++;
    }

    return count;
}

int main()
{
    uint64_t max = 0;
    int number = -1;
    for (int i = 1; i < MAX; i++) {
        uint64_t tmp = apply_collatz(i);
        if (tmp > max) {
            max = tmp;
            number = i;
        }
    }

    printf("number %d has %lu length\n", number, max);

    return 0;
}

