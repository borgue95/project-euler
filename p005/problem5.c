#include <stdio.h>
#include <stdlib.h>

/*
 * check divisibility by:
 * 2, 3, 4, 5, 7, 9, 11, 13, 16, 17, 19
 * no check divisibility by:
 * - 6.  If it is divisible by 2 and 3, it is divisible by 6
 * - 8.  If it is divisible by 2 and 4, it is divisible by 8
 * - 10. If it is divisible by 2 and 5, it is divisible by 10
 * - 12. If it is divisible by 3 and 4, it is divisible by 12
 * - 14. If it is divisible by 2 and 7, it is divisible by 14
 * - 15. If it is divisible by 3 and 5, it is divisible by 15
 * - 18. If it is divisible by 2 and 9, it is divisible by 18
 * - 20. If it is divisible by 4 and 5, it is divisible by 20
 *
 * Take care that 4, 9 and 16, etc have multiple prime numbers in their prime
 * factors. I've included 4 and 9 in the list because i can remove other numbers
 * like 8,12,18 and 20
 *
 * Only works with 1 to 20
 */

int main()
{
    int max = 20;
    int num = 0;

    int flag = 0;

    //int list[11] = {2, 3, 4, 5, 7, 9, 11, 13, 16, 17, 19};

    /*
    int *list = (int *) alloca(sizeof(int) * 11);
    list[0] = 2;
    list[1] = 3;
    list[2] = 4;
    list[3] = 5;
    list[4] = 7;
    list[5] = 9;
    list[6] = 11;
    list[7] = 13;
    list[8] = 16;
    list[9] = 17;
    list[10] = 19;
    */

    while (1) {
        flag = 1;
        //for (int i = 0; i < 11; i++) {
        //    if (num % list[i] != 0) {
        //for (int i = 0; i < 11; i++) {
        //    if (num % *(list+i) != 0) {
        for (int i = 2; i < max; i++) { // 20 = 4*5 (imparells? no)
            if (num % i != 0) {
                flag = 0;
                break;
            }   
        }
        if (flag && num) {
            printf("Smallest number found: %d\n", num);
            break;
        }
        num+=9699690; //2*3*5*7*9*11*13*17*19
    }

    return 0;
}
