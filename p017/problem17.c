#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

enum LowNumbers {
    ZERO = 4,
    ONE = 3,
    TWO = 3,
    THREE = 5,
    FOUR = 4,
    FIVE = 4,
    SIX = 3,
    SEVEN = 5,
    EIGHT = 5,
    NINE = 4,
    TEN = 3,
    ELEVEN = 6,
    TWELVE = 6,
    THIRTEEN = 8,
    FOURTEEN = 8,
    FIVETEEN = 8,
    SIXTEEN = 7,
    SEVENTEEN = 9,
    EIGHTEEN = 8,
    NINETEEN = 8,
};

enum Tenths {
    TWENTY = 6,
    THIRTY = 6,
    FOURTY = 6,
    FIVETY = 6,
    SIXTY = 5,
    SEVENTY = 7,
    EIGHTY = 6,
    NINETY = 6
};

enum Others {
    HUNDRED = 7,
    THOUSAND = 8,
    AND = 3
};

int LowNumbersArr[20] = {
    0, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE,
    TEN, ELEVEN, TWELVE, THIRTEEN, FOURTEEN, FIVETEEN, SIXTEEN, SEVENTEEN,
    EIGHTEEN, NINETEEN
};

int TenthsArr[10] = {
    0, 0, TWENTY, THIRTY, FOURTY, FIVETY, SIXTY, SEVENTY, EIGHTY, NINETY
};

int count_letters(int number)
{
    int tmp = number;

    // breaking up in units, tenths, hundreds, etc
    int units = number % 10;
    tmp /= 10;
    int tenths = tmp % 10;
    tmp /= 10;
    int hundreds = tmp % 10;
    tmp /= 10;
    int thousands = tmp % 10;
    int aux = number % 100;

    //printf("%d %d %d %d\n", thousands, hundreds, tenths, units);

    // start counting
    int counter = 0;
    counter += thousands > 0 ? LowNumbersArr[thousands] : 0;
    counter += thousands > 0 ? THOUSAND : 0;
    counter += hundreds > 0 ? LowNumbersArr[hundreds] : 0;
    counter += hundreds > 0 ? HUNDRED : 0;
    counter += hundreds > 0 && (tenths > 0 || units > 0) ? AND : 0;
    counter += aux < 20 ? LowNumbersArr[aux] : 0;
    counter += aux >= 20 ? TenthsArr[tenths] : 0;
    counter += aux >= 20 ? LowNumbersArr[units] : 0;

    //printf("Number of digits for %d: %d\n", number, counter);

    return counter;
}

int count_letters_test(char *str)
{
    int counter = 0;
    for (; *str; str++) {
        counter += isalpha(*str) ? 1 : 0;
    }
    return counter;
}

void check(char *str, int calculated)
{
    int real = count_letters_test(str);
    if (real != calculated) {
        printf("%s does not match: real %d, calculated %d\n", 
                str, real, calculated);
    }
}

void test()
{
    char *str = (char*) malloc(sizeof(char) * 100);

    sprintf(str, "twenty two");
    check(str, count_letters(22));
    sprintf(str, "one hundred and thirty");
    check(str, count_letters(130));
    sprintf(str, "eight hundred and one");
    check(str, count_letters(801));
    sprintf(str, "sixty six");
    check(str, count_letters(66));
    sprintf(str, "three hundred and sixteen");
    check(str, count_letters(316));
    sprintf(str, "nine");
    check(str, count_letters(9));
    sprintf(str, "nine hundred and ninety nine");
    check(str, count_letters(999));
}

void run_problem()
{
    int counter = 0;
    for (int i = 1; i <= 1000; i++) {
        counter += count_letters(i);
    }
    
    printf("Writing down all numbers from 1 to 1000 will occupy %d letters\n",
            counter);
}

int main(int argc, char **argv)
{
    if (argc > 3) {
        printf("Usage: %s [test]\n", argv[0]);
        exit(1);
    }

    if (argc == 2) {
        test();
    } else {
        run_problem();
    }

    return 0;

}
