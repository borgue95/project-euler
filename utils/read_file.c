#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Reads fixed size lines from a file. 
 * Params: filename, line size, number of lines
 * Returns: list of n lines. Each line has 'size' characters and is not 
 *                           terminated.
 */
char** read_equalsize_lines(const char *filename,
                            int line_size,
                            int num_lines)
{
    FILE *f = fopen(filename, "r");
    if (!f) {
        printf("File %s not found. Exiting\n");
        exit(EXIT_FAILURE);
    }

    char **lines = malloc(sizeof(char*) * num_lines);

    char buff[line_size + 2]; // \n\0

    size_t i = 0;
    while (fgets(buff, line_size + 2, f)) {
        char *line = malloc(sizeof(char) * line_size);
        strncpy(line,buff,line_size);
        lines[i] = line;
        i++;
    }

    return lines;
}
