#ifndef READ_FILE_H_
#define READ_FILE_H_

char** read_equalsize_lines(const char *filename,
                            int line_size,
                            int num_lines);

#endif
