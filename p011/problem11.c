#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MATX 20
#define MATY 20

#define MAX_PROD 4 // carefull with int overflow!

/*
 * -----> X
 * |
 * |
 * v
 * Y
 */

int** parse_file()
{
    FILE *f = fopen("data.txt","r");
    if (!f) {
        printf("File data.txt not found\n");
        exit(EXIT_FAILURE);
    }

    int **matrix = malloc(sizeof(int*) * MATY);
    
    char buff[100];

    int i = 0;
    while(fgets(buff, 100, f)) {
        char *str = strtok(buff, " \n");
        int *row = malloc(sizeof(int*) * MATX);
        int j = 0;
        while (str != NULL) {
            row[j] = atoi(str);
            str = strtok(NULL, " \n");
            j++;
        }
        matrix[i] = row;
        i++;
    }

    /*
    for (int i = 0; i < MATY; i++) {
        for (int j = 0; j < MATX; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
    */

    return matrix;
}

void find_max_product_horizontally(int **matrix)
{
    int max = 0;
    int max_val_x = -1;
    int max_val_y = -1;

    int tmp;
    for (int i = 0; i < MATY; i++) { // for every row
        for (int j = 0; j < MATX - MAX_PROD; j++) {
            tmp = 1;
            for (int k = 0; k < MAX_PROD; k++) {
                tmp *= matrix[i][j+k];
            }
            if (tmp > max) {
                max = tmp;
                max_val_x = j;
                max_val_y = i;
            }
        }
    }

    printf("max mult horizontally: %d. Row %d Col %d. Numbers: ",
            max, max_val_y, max_val_x);
    for (int i = 0; i < MAX_PROD; i++) {
        printf("%d ", matrix[max_val_y][max_val_x+i]);
    }
    printf("\n");
}

void find_max_product_vertically(int **matrix)
{
    int max = 0;
    int max_val_x = -1;
    int max_val_y = -1;

    int tmp;
    for (int j = 0; j < MATX; j++) {
        for (int i = 0; i < MATY - MAX_PROD; i++) { // for every col
            tmp = 1;
            for (int k = 0; k < MAX_PROD; k++) {
                tmp *= matrix[i+k][j];
            }
            if (tmp > max) {
                max = tmp;
                max_val_x = j;
                max_val_y = i;
            }
        }
    }

    printf("max mult vertically: %d. Row %d Col %d. Numbers: ",
            max, max_val_y, max_val_x);
    for (int i = 0; i < MAX_PROD; i++) {
        printf("%d ", matrix[max_val_y+i][max_val_x]);
    }
    printf("\n");
}

/*
 * \
 *  \
 */
void find_max_prodcut_diagonally_down(int **matrix)
{
    int max = 0;
    int max_val_x = -1;
    int max_val_y = -1;

    int tmp;
    for (int i = 0; i <= MATY - MAX_PROD; i++) { // for every row
        for (int j = 0; j <= MATX - MAX_PROD; j++) {
            tmp = 1;
            for (int k = 0; k < MAX_PROD; k++) {
                tmp *= matrix[i+k][j+k];
            }
            if (tmp > max) {
                max = tmp;
                max_val_x = j;
                max_val_y = i;
            }
        }
    }

    printf("max mult diagonally down (\\): %d. Row %d Col %d. Numbers: ",
            max, max_val_y, max_val_x);
    for (int i = 0; i < MAX_PROD; i++) {
        printf("%d ", matrix[max_val_y+i][max_val_x+i]);
    }
    printf("\n");
}

/*
 *  / 
 * / 
 */
void find_max_prodcut_diagonally_up(int **matrix)
{
    int max = 0;
    int max_val_x = -1;
    int max_val_y = -1;

    int tmp;
    for (int i = MAX_PROD - 1; i < MATY; i++) { // for every row
        for (int j = MAX_PROD - 1; j <= MATX; j++) {
            tmp = 1;
            for (int k = 0; k < MAX_PROD; k++) {
                tmp *= matrix[i-MAX_PROD+k+1][j-k];
            }
            if (tmp > max) {
                max = tmp;
                max_val_x = j;
                max_val_y = i-MAX_PROD+1;
            }
        }
    }

    printf("max mult diagonally up (/): %d. Row %d Col %d. Numbers: ",
            max, max_val_y, max_val_x);
    for (int i = 0; i < MAX_PROD; i++) {
        printf("%d ", matrix[max_val_y+i][max_val_x-i]);
    }
    printf("\n");
}

int main()
{
    int **matrix = parse_file();
    find_max_product_horizontally(matrix);
    find_max_product_vertically(matrix);
    find_max_prodcut_diagonally_down(matrix);
    find_max_prodcut_diagonally_up(matrix);
    return 0;
}


