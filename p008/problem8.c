#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define FILE_LENGTH 1000
#define N 13

int can_multiply(int *numbers, int start)
{
    for (int i = start; i < start + N; i++) {
        if (numbers[i] == 0) {
            return 0;
        }
    }
    return 1;
}

int main()
{
    char *characters = malloc(sizeof(char) * (FILE_LENGTH + 2)); // + \n + \0
    int *numbers = malloc(sizeof(int) * FILE_LENGTH);

    FILE *file = fopen("data.txt", "r");
    if (file == NULL) {
        printf("File data.txt not found\n");
        exit(EXIT_FAILURE);
    }
    
    char *success = fgets(characters, FILE_LENGTH, file);
    if (success == NULL) {
        printf("Empty file found...\n");
        exit(EXIT_FAILURE);
    }

    // cast
    for (int i = 0; i < FILE_LENGTH; i++) {
        numbers[i] = (int) characters[i] - 48;
    }

    uint64_t max = 0;
    int max_index = -1;
    for (int i = 0; i < FILE_LENGTH - N; i++) {
        if (can_multiply(numbers, i)) {
            uint64_t tmp = 1;
            for (int j = 0; j < N; j++) {
                tmp *= (uint64_t) numbers[i+j];
            }

            if (tmp > max) {
                max = tmp;
                max_index = i;
            }
        }
    }

    printf("Maximum product: %lu\n", max);
    for (int i = max_index; i < max_index + N - 1; i++) {
        printf("%d * ", numbers[i]);
    }
    printf("%d = %lu\n", numbers[max_index + N - 1], max);

    
    return 0;
}

    
