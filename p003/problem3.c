#include <stdio.h>
#include <stdint.h>
#include <math.h>

int check_primarity(uint64_t n)
{
    uint64_t sqt = (uint64_t) sqrt(n);
    uint64_t i = 3;

    while (n % i && i <= sqt) {
        i+=2;
    }

    return i > sqt;

}

int main()
{
    uint64_t n = 600851475143;
    //n = 13195;
    uint64_t sqt = (uint64_t) sqrt(n);
    if (sqt % 2 == 0) {
        sqt++; // we want an odd number
    }
    uint64_t count = sqt;

    while (1) {
        if (n % count == 0) {
            if (check_primarity(count)) {
                break;
            }
        }
        count -= 2;
    }

    printf("Largest prime factor: %lu\n", count);
    
    return 0;
}
