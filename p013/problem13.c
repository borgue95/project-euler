#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "utils.h"

#define LINE_SIZE 50
#define NUM_LINES 100
#define NUM_DIGITS 12 // during calculation
#define FINAL_NUM_DIGITS 10

void fill_line(char *line, uint64_t *number_line)
{
    int j = LINE_SIZE - NUM_DIGITS; // last 9 digits
    int k = ceil((float)LINE_SIZE / (float)NUM_DIGITS) - 1; // last column of number_line
    char *tmp = malloc(NUM_DIGITS + 1);
    memset(tmp, 0, NUM_DIGITS + 1);
    while (j >= 0) {
        strncpy(tmp, line+j, NUM_DIGITS);
        number_line[k] = atoll(tmp);
        memset(tmp, 0, NUM_DIGITS + 1);
        k--;
        j-=NUM_DIGITS;
    }

    if (j < 0) {
        strncpy(tmp, line, NUM_DIGITS + j);
        number_line[k] = atoll(tmp);
    }
}

int main()
{
    char **lines = read_equalsize_lines("data.txt", 50, 100);

    // every NUM_DIGITS digits in a 32bit uint -> matrix of integers
    // NUM_LINES x (LINE_SIZE / NUM_DIGITS + 1)
    // i -> rows = NUM_LINES
    // j -> columns = 32bit uints
    uint64_t **matrix = malloc(sizeof(uint64_t*) * NUM_LINES);
    int num_cols = (int)ceil((float)LINE_SIZE / (float)NUM_DIGITS);
    for (int i = 0; i < NUM_LINES; i++) {
        matrix[i] = malloc(sizeof(uint64_t) * num_cols);
        fill_line(lines[i], matrix[i]);
    }

    // make the sum
    
    // accumulators (for each column)
    uint64_t *accum = calloc(sizeof(uint64_t), num_cols);
    for (int i = 0; i < NUM_LINES; i++) {
        for (int j = 0; j < num_cols; j++) {
            accum[j] += matrix[i][j];
        }
    }

    // fix the carries (except for the most significative number)
    /* I will show the final result in base 10, so i cannot do bitwise
     * operations. The chunks are displayed in base 10, regardless of the 
     * later and previous bits are
     */
    for (int j = num_cols - 1; j > 0; j--) {
        uint64_t carry = accum[j] / (uint64_t) pow(10,NUM_DIGITS);
        accum[j-1] += carry;
        accum[j] %= (uint64_t) pow(10,NUM_DIGITS);
    }

    // print the hole number
    printf("%lu", accum[0]);
    for (int j = 1; j < num_cols; j++) {
        printf("%09lu", accum[j]);
    }
    printf("\n");

    // print only the first 10 digits
    char *all_digits = calloc(sizeof(char), FINAL_NUM_DIGITS + 1);
    char *first_digits = calloc(sizeof(char), NUM_DIGITS + 1);
    sprintf(first_digits, "%lu", accum[0]);
    strcat(all_digits, first_digits);
    int i = 1;
    while (strlen(all_digits) < FINAL_NUM_DIGITS) {
        sprintf(first_digits, "%09lu", accum[i]);
        strncat(all_digits, first_digits, FINAL_NUM_DIGITS - strlen(all_digits));
        i++;
    }

    printf("%s\n", all_digits);

    return 0;
}

