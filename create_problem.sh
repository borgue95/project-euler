#!/bin/bash
if [ -z $1 ]; 
then 
     echo Usage: $0 problem_number
     exit -1
fi

problem_number=$(echo $1 | awk '{printf "%03d", $1}')

directory=p$problem_number

if [ -d $directory ];
then 
    echo This problem arleady exists. exiting
    exit 0
fi

mkdir $directory

sed s/problem/problem$1/g baseMakefile > $directory/Makefile
touch $directory/problem$1.c

