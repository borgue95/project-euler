#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// 18 minutes
#define MAX_H 20 // j
#define MAX_V 20 // i

void recursive_call(uint64_t *count, int i, int j)
{
    if (i < MAX_V) {
        recursive_call(count,i+1,j);
    }
    if (j < MAX_H) {
        recursive_call(count,i,j+1);
    }
    if (i == MAX_V && j == MAX_H) {
        (*count)++;
    }
}

int main()
{
    uint64_t count = 0;
    recursive_call(&count, 0, 0);

    printf("count: %lu\n", count);

    return 0;
}
