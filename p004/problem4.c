#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int is_palindrom(int n)
{
    // count digits
    int n_digits = 1;
    int tmp = n;
    while (tmp / 10) {
        n_digits++;
        tmp /= 10;
    }

    // check
    int index = 0;
    while (index <= ceil((float)n_digits / 2.0)) {
        if (!((n / (int)pow(10, n_digits - index - 1)) % 10 == 
              (n / (int)pow(10, index)) % 10)) {
            return 0;
        }
        index++;
    }

    return 1;
}

int main()
{
    for (int i = 999; i >= 100; i--) {
        for (int j = 999; j >= 100; j--) {
            if (is_palindrom(i * j)) {
                printf("Two numbers: %d * %d = %d\n", i, j, i * j);
                return 0;
            }
        }
    }
    
    return 0;
}
