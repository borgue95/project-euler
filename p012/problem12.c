#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define MAX 500

uint64_t create_triangular_number(uint32_t n)
{
    /*
    uint64_t accum = 0;
    for (uint32_t i = 1; i <= n; i++) {
        accum += i;
    }
    */
    uint64_t accum = (n * (n + 1)) / 2;
    return accum;
}

// 17 min
int find_divisors_from_number_naive(uint64_t n)
{
    uint64_t accum = 1;
    int count = 0;

    while (accum <= n) {
        if (n % accum == 0) {
            count++;
        }
        accum++;
    }

    return count;
}

// 0.336 secs
int find_divisors_from_number_op1(uint64_t n)
{
    uint64_t accum = 1;
    int count = 0;
    uint64_t sqrt_n = ceil(sqrt(n));

    while (accum <= sqrt_n) {
        if (n % accum == 0) {
            if (n % accum == accum) {
                count++;
            } else {
                count+=2;
            }
        }
        accum++;
    }

    return count;
}

int main()
{
    int max_divisors = 0;
    uint32_t i = 1;
    uint64_t triag;
    int count;

    while (max_divisors <= MAX) { // OVER five hundred
        triag = create_triangular_number(i);
        count = find_divisors_from_number_op1(triag);
        if (count > max_divisors) {
            max_divisors = count;
        }
        i++;
    }
    printf("%dth triangular number = %lu and has %d divisors\n", i, triag, count);
    return 0;
}
